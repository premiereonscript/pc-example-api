# Procore Sample API

Developed by Eli Portell

### About

After learning about the services Procore provides to its customers, I thought it could make a good demo of my skills to recreate some of these services via an API. Some examples of tasks this API can handle are adding users to/from a company, adding projects, adding punchlist items, adding project contributors, retrieving all company projects, users, and punchlist items, and more...

This API uses express.js, Google’s Firebase cloud functions, and Firebase authentication to expose read/write operations on a Google Cloud MySQL database.

The services provided are tied closely to the user's authorization and company association. If you need a sample user to test with, I recommend logging in with the credentials `emusk@tesla.biz pw:tesla123`

## EndPoints

### Base URL -

`https://us-central1-pc-sample-api.cloudfunctions.net/api/v1`

### Root `[ / ]`

`GET [ / ]` - Returns a freindly welcome message

### Users

#### Signup `POST [/Users]`

- Body Example
  `{ "email": "eMusk@tesla.biz", "password": "tesla123", "confirmPassword": "tesla123", "firstName": "Elon", "lastName": "Musk", "admin": true }`
- Response
  `authorizationToken`

#### Login `POST [/Users/Login]`

- Body Example
  `{ "email": "eMusk@tesla.biz", "password": "tesla123"}`
- Response
  `authorizationToken`

#### Remove a user from a company `PUT [/Users/Remove]`

- Authorization header required
- Body Example
  `{ "email": "eMusk@tesla.biz"`
- Response
  ` {"message": "User eMusk@tesla.biz has been removed from company"}`

#### Add a user to a company `PUT [/Users/Add]`

- Authorization header required
- Body Example
  `{ "email": "eMusk@tesla.biz"}`
- Response
  ` {"message": "User eMusk@tesla.biz has added to company 1001"}`

#### Get all users from requesting user's company `GET [/Users/Add]`

- Authorization header required
- Body Example
  `{ "email": "eMusk@tesla.biz"}`
- Response List
  `[{"FirstName": "Eli", "LastName": "Portell", "Email": "eliportell@hotmail.com"}]`

### Companies

#### Add Company `POST [/Companies]`

- Body Example
  `{ "name": "Tesla", "userMax": 250, "street": "269 N Lomita Ave", "city": "Ojai", "state": "CA" }`
- Response
  `{"companyId": 1007 }`

#### Get All Companies `GET [/Companies]`

- Response List
  `{"companies": [{ "CompanyId": 1001, "Name": "Procore", "Street": "6309 Carpinteria Ave", "City": "Carpinteria", "State": "CA" }] }`

#### Get Company by ID `GET [/Companies/:CompanyId]`

- Response List
  `{"companies": [{ "CompanyId": 1001, "Name": "Procore", "Street": "6309 Carpinteria Ave", "City": "Carpinteria", "State": "CA" }] }`

### Projects

#### Get all company projects `GET [/Projects]`

- Authorization header required
- Response List
  `["projects": [ { "Name": "Contruction Site 2", "Role": "Owner", "ProjectId": "60bfabb0-3dae-11eb-a310-c322d96beba3" }]`

#### Create new project `POST [/Projects]`

- Authorization header required
- Body Example
  `{ "name": "New football stadium", "street": "89 Third Ave", "city": "Carpinteria", "state": "CA" }`
- Response
  `{"projectId": "60bfabb0-3dae-11eb-a310-c322d96beba3" }`

#### Add company to project `POST [/Projects/:ProjectId]`

- Authorization header required
- Body Example
  `{ "role": "Architect", "group": 2, "companyId": 1003 }`
- Response
  `{message: Company 1004 added to project 60bfabb0-3dae-11eb-a310-c322d96beba3 }`

### PunchLists

#### Add task to project punchlist `POST [/Projects/:ProjectId/Punchlist]`

- Authorization header required
- Body Example
  `{ "title": "Dent in the wall", "description": "Dent in the wall by the basketball court.", "companyId": 1001 }`
- Response
  `{message: New Punchlist item created at id: 400bfsdfb0-3dae-11eb-a310-c322d96bebca2}`

#### Get all punchlist items `GET [/Projects/:ProjectId/Punchlist]`

- Authorization header required
- Response List
  `{[{ "Title": "Unfinished concete pour", "Description": "Concrete does was not poured to full boundary.", "Status": "Open", "DateSubmitted": "2020-12-14T03:46:33.000Z", "OriginatingUser": "eliportell@hotmail.com", "AssignedTo": 1001 }]}`

## Data Model

![picture](QuickDBD-Procore_Sample.png)
