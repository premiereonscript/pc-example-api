﻿-- Exported from QuickDBD: https://www.quickdatabasediagrams.com/
-- Link to schema: https://app.quickdatabasediagrams.com/#/d/N8VHEJ
-- NOTE! If you have used non-SQL datatypes in your design, you will have to change these here.


CREATE TABLE `USERS` (
    `Email` VARCHAR(255) NOT NULL  ,
    `FirstName` VARCHAR(255) NOT NULL  ,
    `LastName` VARCHAR(255) NOT NULL ,
    `Admin` boolean NOT NULL,
    `CompanyId` int,
    `FirebaseId` VARCHAR(255),
    PRIMARY KEY (
        `Email`
    )
);

CREATE TABLE `COMPANIES` (
    `CompanyId` int  NOT NULL ,
    `Name` VARCHAR(255)  NOT NULL ,
    `Street` VARCHAR(255) ,
    `City` VARCHAR(255)  ,
    `State` VARCHAR(255)  ,
    `UserMax` int  NOT NULL ,
    `UserCount` int  NOT NULL ,
    PRIMARY KEY (
        `CompanyId`
    )
);

CREATE TABLE `PROJECT_ROLES` (
    `RoleId` VARCHAR(255)   NOT NULL ,
    `Role` VARCHAR(255)   NOT NULL ,
    `PermissionGroup` int  NOT NULL ,
    `ProjectId` VARCHAR(255)  NOT NULL ,
    `CompanyID` int  NOT NULL ,
    PRIMARY KEY (
        `RoleId`
    )
);

CREATE TABLE `PROJECTS` (
    `ProjectId` VARCHAR(255)  NOT NULL ,
    `Name` VARCHAR(255)  NOT NULL ,
    `Street` VARCHAR(255)   ,
    `City` VARCHAR(255)   ,
    `State` VARCHAR(255)   ,
    `Phase` VARCHAR(255)  NOT NULL ,
    PRIMARY KEY (
        `ProjectId`
    )
);

CREATE TABLE `CONTRACTS` (
    `ContractId` VARCHAR(255)  NOT NULL ,
    `ProjectId` VARCHAR(255)   NOT NULL ,
    `DateSubmitted` datetime  NOT NULL ,
    `Type` VARCHAR(255)  NOT NULL ,
    `Amount` int  NOT NULL ,
    `Status` VARCHAR(255)  NOT NULL ,
    `OriginatingCompany` int  NOT NULL ,
    `RecievingCompany` int  NOT NULL ,
    PRIMARY KEY (
        `ContractId`
    )
);

CREATE TABLE `RFIS` (
    `RfiId` VARCHAR(255)  NOT NULL ,
    `DateSubmitted` datetime  NOT NULL ,
    `Title` VARCHAR(255)  NOT NULL ,
    `Question` VARCHAR(255)  NOT NULL ,
    `Urgency` int  NOT NULL ,
    `DateResponded` datetime  NOT NULL ,
    `Response` VARCHAR(255)  NOT NULL ,
    `OriginatingUser` VARCHAR(255)  NOT NULL ,
    `RespondingUser` VARCHAR(255)  NOT NULL ,
    `AssignedTo` int  NOT NULL ,
    `ProjectId` int  NOT NULL ,
    PRIMARY KEY (
        `RfiId`
    )
);

CREATE TABLE `PUNCHLISTS` (
    `PunchListId` VARCHAR(255)  NOT NULL ,
    `ProjectId` VARCHAR(255)   NOT NULL ,
    PRIMARY KEY (
        `PunchListId`
    )
);

CREATE TABLE `PUNCHLIST_TASKS` (
    `TaskId` int  NOT NULL ,
    `Title` VARCHAR(255)  NOT NULL ,
    `Description` VARCHAR(255)  NOT NULL ,
    `DateSubmitted` datetime  NOT NULL ,
    `Status` VARCHAR(255)  NOT NULL ,
    `Notes` VARCHAR(255)  NOT NULL ,
    `PunchListId` VARCHAR(255)  NOT NULL ,
    `OriginatingUser` VARCHAR(255)  NOT NULL ,
    `FinishingUser` VARCHAR(255)  NOT NULL ,
    `AssignedTo` int  NOT NULL ,
    PRIMARY KEY (
        `TaskId`
    )
);

ALTER TABLE `USERS` ADD CONSTRAINT `fk_USERS_CompanyId` FOREIGN KEY(`CompanyId`)
REFERENCES `COMPANIES` (`CompanyId`);

ALTER TABLE `PROJECT_ROLES` ADD CONSTRAINT `fk_PROJECT_ROLES_ProjectId` FOREIGN KEY(`ProjectId`)
REFERENCES `PROJECTS` (`ProjectId`);

ALTER TABLE `PROJECT_ROLES` ADD CONSTRAINT `fk_PROJECT_ROLES_CompanyID` FOREIGN KEY(`CompanyID`)
REFERENCES `COMPANIES` (`CompanyId`);

ALTER TABLE `CONTRACTS` ADD CONSTRAINT `fk_CONTRACTS_ProjectId` FOREIGN KEY(`ProjectId`)
REFERENCES `PROJECTS` (`ProjectId`);

ALTER TABLE `CONTRACTS` ADD CONSTRAINT `fk_CONTRACTS_OriginatingCompany` FOREIGN KEY(`OriginatingCompany`)
REFERENCES `COMPANIES` (`CompanyId`);

ALTER TABLE `CONTRACTS` ADD CONSTRAINT `fk_CONTRACTS_RecievingCompany` FOREIGN KEY(`RecievingCompany`)
REFERENCES `COMPANIES` (`CompanyId`);

ALTER TABLE `RFIS` ADD CONSTRAINT `fk_RFIS_OriginatingUser` FOREIGN KEY(`OriginatingUser`)
REFERENCES `USERS` (`Email`);

ALTER TABLE `RFIS` ADD CONSTRAINT `fk_RFIS_RespondingUser` FOREIGN KEY(`RespondingUser`)
REFERENCES `USERS` (`Email`);

ALTER TABLE `RFIS` ADD CONSTRAINT `fk_RFIS_AssignedTo` FOREIGN KEY(`AssignedTo`)
REFERENCES `COMPANIES` (`CompanyId`);

ALTER TABLE `RFIS` ADD CONSTRAINT `fk_RFIS_ProjectId` FOREIGN KEY(`ProjectId`)
REFERENCES `COMPANIES` (`CompanyId`);

ALTER TABLE `PUNCHLISTS` ADD CONSTRAINT `fk_PUNCHLISTS_ProjectId` FOREIGN KEY(`ProjectId`)
REFERENCES `PROJECTS` (`ProjectId`);

ALTER TABLE `PUNCHLIST_TASKS` ADD CONSTRAINT `fk_PUNCHLIST_TASKS_PunchListId` FOREIGN KEY(`PunchListId`)
REFERENCES `PUNCHLISTS` (`PunchListId`);

ALTER TABLE `PUNCHLIST_TASKS` ADD CONSTRAINT `fk_PUNCHLIST_TASKS_OriginatingUser` FOREIGN KEY(`OriginatingUser`)
REFERENCES `USERS` (`Email`);

ALTER TABLE `PUNCHLIST_TASKS` ADD CONSTRAINT `fk_PUNCHLIST_TASKS_FinishingUser` FOREIGN KEY(`FinishingUser`)
REFERENCES `USERS` (`Email`);

ALTER TABLE `PUNCHLIST_TASKS` ADD CONSTRAINT `fk_PUNCHLIST_TASKS_AssignedTo` FOREIGN KEY(`AssignedTo`)
REFERENCES `COMPANIES` (`CompanyId`);

