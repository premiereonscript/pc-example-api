const functions = require('firebase-functions');
const express = require('express');
const app = express();
const description = require('./util/desc');

const config = require('./util/config');
const firebase = require('firebase');
firebase.initializeApp(config);

const {
	signup,
	login,
	addToCompany,
	removeFromCompany,
	getAllUsersFromCompany,
} = require('./handlers/users');
const {
	addCompany,
	getAllCompanies,
	getCompany,
} = require('./handlers/companies');
const { userAuth, isAdmin, linkCompany } = require('./middleware/auth');
const {
	newProject,
	getAllCompanyProjects,
	addCompanyToProject,
} = require('./handlers/projects');
const { addPunchlistItem, getAllPunchlistItems } = require('./handlers/punchlists');

// Establish nested routes
const baseRouter = express.Router();
const userRouter = express.Router({ mergeParams: true });
const companyRouter = express.Router({ mergeParams: true });
const projectsRouter = express.Router({ mergeParams: true });
app.use('/v1', baseRouter);
baseRouter.use('/Users', userRouter);
baseRouter.use('/Companies', companyRouter);
baseRouter.use('/Projects', projectsRouter);

// Base Route
baseRouter.get('/', (req, res) => {
	res.send(description.text);
});

// /Users Routes
userRouter.post('/', signup);
userRouter.post('/Login', login);
userRouter.put('/Add', userAuth, isAdmin, addToCompany);
userRouter.put('/Remove', userAuth, isAdmin, removeFromCompany);
userRouter.get('/Company', userAuth, getAllUsersFromCompany);

// /Companies Routes
companyRouter.post('/', addCompany);
companyRouter.get('/', getAllCompanies);
companyRouter.get('/:CompanyId', getCompany);

// /Projects Routes
projectsRouter.get('/', userAuth, linkCompany, getAllCompanyProjects);
projectsRouter.post('/', userAuth, isAdmin, newProject);
projectsRouter.post('/:ProjectId', userAuth, isAdmin, addCompanyToProject);
projectsRouter.post(
	'/:ProjectId/Punchlist',
	userAuth,
	linkCompany,
	addPunchlistItem
);
projectsRouter.get(
	'/:ProjectId/Punchlist',
	userAuth,
	linkCompany,
	getAllPunchlistItems
);

exports.api = functions.https.onRequest(app);
