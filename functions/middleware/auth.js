const functions = require('firebase-functions');
const { admin } = require('../util/admin');

const firebase = require('firebase');

exports.userAuth = async (req, res, next) => {
	try {
		let idToken;
		if (
			req.headers.authorization &&
			req.headers.authorization.startsWith('Bearer ')
		) {
			idToken = req.headers.authorization.split('Bearer ')[1];
		} else {
			console.error('No token found');
			return res.status(403).json({ error: 'Unauthorized' });
		}

		let decodedToken = await admin.auth().verifyIdToken(idToken);

		if (!decodedToken) {
			return res.status(403).json({ error: 'Unauthorized' });
		}
		req.user = decodedToken;

		const db = await require('../util/db_connect')();
		let dbUser = await db.raw(
			`SELECT Email FROM USERS
        WHERE FirebaseId = "${req.user.uid}"`
		);

		if (dbUser[0].length == 0) {
			res.status(401).json({
				message: `Authorization failed`,
			});
		}
		req.user.email = dbUser[0][0]['Email'];
		functions.logger.log('User authenticated at email: ', req.user.email);

		return next();
	} catch (err) {
		console.error('Error while verifying token ', err);
		return res.status(403).json(err);
	}
};

exports.isAdmin = async (req, res, next) => {
	const db = await require('../util/db_connect')();
	let adminResult = await db.raw(
		`SELECT Admin, CompanyId FROM USERS
        WHERE Email = "${req.user.email}"`
	);

	if (!adminResult[0][0]['Admin']) {
		res.status(403).json({
			message: `Your account is not authorized to perform this action`,
		});
	}
	req.user.admin = true;
	req.user.companyId = adminResult[0][0]['CompanyId'];
	next();
};

exports.linkCompany = async (req, res, next) => {
	const db = await require('../util/db_connect')();
	let adminResult = await db.raw(
		`SELECT CompanyId FROM USERS
        WHERE Email = "${req.user.email}"`
	);

	req.user.companyId = adminResult[0][0]['CompanyId'];
	next();
};
