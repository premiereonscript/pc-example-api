const functions = require('firebase-functions');

module.exports = async () => {
	const config = functions.config();
	const knex = await require('knex')({
		client: 'mysql',
		connection: {
			user: config.pc_sample_db.user,
			password: config.pc_sample_db.password,
			database: config.pc_sample_db.db,
			socketPath: config.pc_sample_db.socketpath,
		},
	});
	return knex;
};
