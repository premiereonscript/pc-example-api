const functions = require('firebase-functions');
const { isEmpty } = require('../util/validate');

const firebase = require('firebase');

exports.addCompany = async (req, res) => {
	try {
		const newCo = {
			name: req.body.name,
			userMax: req.body.userMax,
			street: req.body.street,
			city: req.body.city,
			state: req.body.state,
		};

		// Validate new company data
		let errors = {};
		// Validate name
		if (isEmpty(newCo.name)) errors.firstName = 'Must not be empty';

		//Simple validation on address
		if (isEmpty(newCo.street)) errors.street = 'Must not be empty';
		if (isEmpty(newCo.city)) errors.city = 'Must not be empty';
		if (isEmpty(newCo.state)) errors.state = 'Must not be empty';

		// Validate max
		if (!Number.isInteger(newCo.userMax)) errors.max = 'Must be an number';

		// Validate that id has not already been used
		const db = await require('../util/db_connect')();
		let maxId = await db.raw(
			'SELECT MAX(CompanyId) as largestId FROM COMPANIES'
		);

		let incrementId = maxId[0][0]['largestId'];
		incrementId++;

		if (Object.keys(errors).length > 0) {
			return res.status(400).json(errors);
		}

		await db('COMPANIES').insert({
			CompanyId: incrementId,
			Name: newCo.name,
			UserMax: newCo.userMax,
			Street: newCo.street,
			City: newCo.city,
			State: newCo.state,
			UserCount: 0,
		});

		return res.status(201).json({
			companyId: incrementId,
		});
	} catch (err) {
		console.error(err);
		return res.status(500).json({ error: err.code });
	}
};

exports.getAllCompanies = async (req, res) => {
	try {
		const db = await require('../util/db_connect')();
		let companies = await db.raw(
			'Select CompanyId, Name, Street, City, State from COMPANIES'
		);
		return res.status(200).json({
			companies: companies[0],
		});
	} catch (err) {
		console.error(err);
		return res.status(500).json({ error: err.code });
	}
};

exports.getCompany = async (req, res) => {
	try {
		let companyId = req.params.CompanyId;
		const db = await require('../util/db_connect')();
		let companies = await db.raw(
			`Select CompanyId, Name, Street, City, State from COMPANIES
            WHERE CompanyId = ${companyId}`
		);

		if (companies[0].length == 0) {
			res.status(404).json({
				message: `No copmany found with id ${companyId}`,
			});
		}
		return res.status(200).json({
			companies: companies[0],
		});
	} catch (err) {
		console.error(err);
		return res.status(500).json({ error: err.code });
	}
};
