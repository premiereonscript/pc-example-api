const { v1 } = require('uuid');

exports.addPunchlistItem = async (req, res) => {
	try {
		const db = await require('../util/db_connect')();
		// Check for valid ProjectId in url
		let projectCheck = await db.raw(
			`SELECT EXISTS(SELECT * FROM PROJECTS WHERE ProjectId = "${req.params.ProjectId}") AS Checker`
		);
		if (projectCheck[0][0]['Checker'] == 0)
			return res.status(404).json({ message: 'project id not found' });

		if (!isUserCompanyAssociated(req.user.companyId, req.params.ProjectId)) {
			return res.status(403).json({ message: 'not authorized' });
		}

		// Create new punchlist if one hasn't been created already
		let punchListCheck = await db.raw(
			`SELECT EXISTS(SELECT * FROM PUNCHLISTS WHERE ProjectId = "${req.params.ProjectId}") AS Checker`
		);
		let punchlistId;
		if (punchListCheck[0][0]['Checker'] == 0) {
			punchlistId = v1();
			await db('PUNCHLISTS').insert({
				PunchListId: punchlistId,
				ProjectId: req.params.ProjectId,
			});
		} else {
			punchlist = await db.raw(
				`SELECT PunchListId FROM PUNCHLISTS WHERE ProjectId = "${req.params.ProjectId}"`
			);
			punchlistId = punchlist[0][0]['PunchListId'];
		}

		let punchlistTask = {
			TaskId: v1(),
			Title: req.body.title,
			Description: req.body.description,
			DateSubmitted: new Date(),
			Status: 'Open',
			Notes: '',
			PunchListId: punchlistId,
			OriginatingUser: req.user.email,
			FinishingUser: req.user.email,
			AssignedTo: req.body.companyId,
		};
		await db('PUNCHLIST_TASKS').insert(punchlistTask);

		return res.status(201).json({
			message: `New Punchlist item created at id: ${punchlistTask.TaskId}`,
		});
	} catch (err) {
		console.error(err);
		return res.status(500).json({ error: err.code });
	}
};

exports.getAllPunchlistItems = async (req, res) => {
	try {
		const db = await require('../util/db_connect')();

		let projectCheck = await db.raw(
			`SELECT EXISTS(SELECT * FROM PROJECTS WHERE ProjectId = "${req.params.ProjectId}") AS Checker`
		);
		if (projectCheck[0][0]['Checker'] == 0)
			return res.status(404).json({ message: 'project id not found' });

		if (!isUserCompanyAssociated(req.user.companyId, req.params.ProjectId)) {
			return res.status(403).json({ message: 'not authorized' });
		}

		let punchlistItems = await db.raw(
			`SELECT Title, Description, Status, DateSubmitted, OriginatingUser, AssignedTo FROM PUNCHLIST_TASKS
            JOIN PUNCHLISTS ON PUNCHLIST_TASKS.PunchListId = PUNCHLISTS.PunchListId 
            WHERE PUNCHLISTS.ProjectId = "${req.params.ProjectId}" `
		);

		return res.status(200).json(punchlistItems[0]);
	} catch (err) {
		console.error(err);
		return res.status(500).json({ error: err.code });
	}
};

const isUserCompanyAssociated = async (companyId, projectId) => {
	const db = await require('../util/db_connect')();
	let companyCheck = await db.raw(
		`SELECT EXISTS(SELECT * FROM PROJECT_ROLES WHERE ProjectId = "${projectId}" AND CompanyId=${companyId}) AS Checker`
	);
	if (companyCheck[0][0]['Checker'] == 0) {
		return false;
	} else {
		return true;
	}
};
