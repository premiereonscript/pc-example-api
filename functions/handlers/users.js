const { admin } = require('../util/admin');
const { isEmpty, isEmail } = require('../util/validate');
const functions = require('firebase-functions');

const firebase = require('firebase');

exports.signup = async (req, res) => {
	try {
		const newUser = {
			email: req.body.email,
			password: req.body.password,
			confirmPassword: req.body.confirmPassword,
			firstName: req.body.firstName,
			lastName: req.body.lastName,
			admin: req.body.admin,
			firebaseId: null,
		};

		// Validate new user data
		let errors = {};
		// Validate email
		if (isEmpty(newUser.email)) {
			errors.email = 'Must not be empty';
		} else if (!isEmail(newUser.email)) {
			errors.email = 'Must be a valid email address';
		}
		// Validate passwords
		if (isEmpty(newUser.password)) errors.password = 'Must not be empty';
		if (newUser.password !== newUser.confirmPassword)
			errors.password = 'Passwords must match.';

		// Validate names
		if (isEmpty(newUser.firstName)) errors.firstName = 'Must not be empty';
		if (isEmpty(newUser.lastName)) errors.lastName = 'Must not be empty';
		// Validate boolean
		if (newUser.admin == null) errors.admin = 'Must not be empty';

		// Validate that email has not already been used
		const db = await require('../util/db_connect')();
		let previousUsers = await db('USERS').where({
			Email: newUser.email,
		});
		if (previousUsers.length > 0)
			errors.alreadyTaken = 'Email address has already been registered.';

		if (Object.keys(errors).length > 0) {
			return res.status(400).json(errors);
		}

		let userCredentials = await firebase
			.auth()
			.createUserWithEmailAndPassword(newUser.email, newUser.password);
		let authToken = await userCredentials.user.getIdToken();
		newUser.firebaseId = userCredentials.user.uid;

		await db('USERS').insert({
			Email: newUser.email,
			FirstName: newUser.firstName,
			LastName: newUser.lastName,
			Admin: newUser.admin,
			FirebaseId: newUser.firebaseId,
		});

		return res.status(201).json({ token: authToken });
	} catch (err) {
		console.error(err);
		return res.status(500).json({ error: err.code });
	}
};

exports.login = async (req, res) => {
	try {
		const user = {
			email: req.body.email,
			password: req.body.password,
		};

		//Validate
		let errors = {};

		if (isEmpty(user.password)) errors.password = 'Must not be empty';
		if (isEmpty(user.email)) errors.email = 'Must not be empty';

		if (Object.keys(errors).length > 0) {
			return res.status(400).json(errors);
		}

		let userCredentials = await firebase
			.auth()
			.signInWithEmailAndPassword(user.email, user.password);
		let authToken = await userCredentials.user.getIdToken();

		return res.status(200).json({ token: authToken });
	} catch (err) {
		console.error(err);
		return res.status(500).json({ error: err.code });
	}
};

exports.addToCompany = async (req, res) => {
	try {
		let params = {
			email: req.body.email,
		};

		//Validate
		let errors = {};
		if (isEmpty(params.email)) errors.email = 'Must not be empty';
		else if (!isEmail(params.email)) {
			errors.email = 'Must be a valid email address';
		}

		const db = await require('../util/db_connect')();

		let users = await db.raw(
			`Select CompanyId from USERS
			WHERE Email = "${params.email}"`
		);

		if (
			users[0][0]['CompanyId'] != null &&
			users[0][0]['CompanyId'] != req.user.companyId
		) {
			res.status(400).json({
				message: `User is already assigned to a company. Remove them from their current company before adding a new association`,
			});
		}

		await db.raw(
			`UPDATE USERS
		SET CompanyId = ${req.user.companyId}
		WHERE Email = "${params.email}"`
		);
		await db.raw(
			`UPDATE COMPANIES
			SET UserCount = UserCount+1
			WHERE UserCount = ${req.user.companyId}`
		);

		return res.status(200).json({
			message: `User ${params.email} has been added to company ${req.user.companyId}`,
		});
	} catch (err) {
		console.error(err);
		return res.status(500).json({ error: err.code });
	}
};

exports.removeFromCompany = async (req, res) => {
	try {
		let params = {
			email: req.body.email,
		};

		//Validate
		let errors = {};
		if (isEmpty(params.email)) errors.email = 'Must not be empty';
		else if (!isEmail(params.email)) {
			errors.email = 'Must be a valid email address';
		}

		const db = await require('../util/db_connect')();

		await db.raw(
			`UPDATE USERS
			SET CompanyId = NULL
			WHERE Email = "${params.email}"`
		);
		await db.raw(
			`UPDATE COMPANIES
			SET UserCount = UserCount-1
			WHERE CompanyId = ${req.user.companyId}`
		);

		return res.status(200).json({
			message: `User ${params.email} has been removed from company`,
		});
	} catch (err) {
		console.error(err);
		return res.status(500).json({ error: err.code });
	}
};

exports.getAllUsersFromCompany = async (req, res) => {
	try {
		let requestorsEmail = req.user.email;

		const db = await require('../util/db_connect')();

		let teamMembers = await db.raw(
			`SELECT FirstName, LastName, Email FROM USERS
			WHERE CompanyId = (SELECT CompanyId from USERS WHERE Email = "${requestorsEmail}")`
		);

		return res.status(200).json({
			teamMembers: teamMembers[0],
		});
	} catch (err) {
		console.error(err);
		return res.status(500).json({ error: err.code });
	}
};
