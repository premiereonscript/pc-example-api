const { admin } = require('../util/admin');
const { isEmpty, isEmail } = require('../util/validate');
const functions = require('firebase-functions');
const { v1 } = require('uuid');

exports.newProject = async (req, res) => {
	try {
		let project = {
			ProjectId: v1(),
			Name: req.body.name,
			Street: req.body.street,
			City: req.body.city,
			State: req.body.state,
			Phase: 'pre-planning',
		};

		const db = await require('../util/db_connect')();

		await db('PROJECTS').insert(project);
		await db('PROJECT_ROLES').insert({
			RoleId: v1(),
			Role: 'Owner',
			PermissionGroup: 1,
			ProjectId: project.ProjectId,
			CompanyId: req.user.companyId,
		});

		return res.status(201).json({
			projectId: project.ProjectId,
		});
	} catch (err) {
		console.error(err);
		return res.status(500).json({ error: err.code });
	}
};

exports.getAllCompanyProjects = async (req, res) => {
	try {
		let companyId = req.user.companyId;

		const db = await require('../util/db_connect')();

		let projects = await db.raw(
			`SELECT COMPANIES.Name, PROJECTS.Name, PROJECT_ROLES.Role, PROJECTS.ProjectId FROM COMPANIES 
            JOIN PROJECT_ROLES ON COMPANIES.CompanyId = PROJECT_ROLES.CompanyId 
            JOIN PROJECTS On PROJECT_ROLES.ProjectId = PROJECTS.ProjectId
            WHERE COMPANIES.CompanyId = "${companyId}"`
		);

		return res.status(200).json({
			projects: projects[0],
		});
	} catch (err) {
		console.error(err);
		return res.status(500).json({ error: err.code });
	}
};

exports.addCompanyToProject = async (req, res) => {
	try {
		let companyId = req.user.companyId;

		let role = {
			RoleId: v1(),
			Role: req.body.role,
			PermissionGroup: req.body.group,
			ProjectId: req.params.ProjectId,
			CompanyId: req.body.companyId,
		};

		const db = await require('../util/db_connect')();
		let errors = {};
		let companyCheck = await db.raw(
			`SELECT EXISTS(SELECT * FROM COMPANIES WHERE CompanyId = "${role.CompanyId}") AS Checker`
		);
		let projectCheck = await db.raw(
			`SELECT EXISTS(SELECT * FROM PROJECTS WHERE ProjectId = "${role.ProjectId}") AS Checker`
		);

		if (projectCheck[0][0]['Checker'] == 0)
			errors.project = 'Could not find project';
		if (companyCheck[0][0]['Checker'] == 0)
			errors.company = 'Could not find copmany';

		if (Object.keys(errors).length > 0) {
			return res.status(404).json(errors);
		}

		await db('PROJECT_ROLES').insert(role);

		return res.status(201).json({
			message: `Company ${role.CompanyId} added to project ${role.ProjectId}`,
		});
	} catch (err) {
		console.error(err);
		return res.status(500).json({ error: err.code });
	}
};
